> **THIS IS CURRENTLY OUT OF DATE**

#Dashboard 2.0

Dashboard is a replacement for apps like GeekTools and Ubersicht. These are awesome apps and I have been using them for years. But the way that Mac OsX handles desktops and spaces there is no way to get different widgets for different desktops just different monitors/screens.

This is solved by periodically generating a custom desktop background image that is applied to a specific desktop on whatever monitor/screen you like. The background image is then changed every X-minutes using the built in random background feature in OsX. I am also using it for my Amazon Kindle as a custom IOT-dashboard engine.

It sure has it's quirks but it works for what I want to accomplish.

_One of the main features of version 2 is that there is no need to prerender the HTML as in V1. Now everything is done in the browser with VUE.js. This also makes the preview/layout work much faster._

##Requriements

- Some kind of unix machine
- Chrome Browser (used for rendering images)
- Some basic scripting skills

##How it works

- You create HTML-file that is the base for your dashboard
- A crontabbed bash-script runs every X-minutes that
    - Generates the desktop images
    - Removes all old prerendered images
    - Places two copies of the newly rendered desktop image in your specified wallpaper folder _this is to make sure that OsX changes images_
    - Your operating system updates the background image every X-minutes

##Installation

- Clone this repository `git clone ...` 
- Create folders `config`,`layouts` and `output`.
- Create another folder structure for your wallpapers. This folder does not have to be in the project folder. I use `~/Documents/Wallpapers`. Create one folder per desktop image you want to create. _Avoid spaces in your folder names._
    - `macbook-air` - main display
    - `macbook-air-someday` - secondary display
    - `dev` - test dashboard
- Create a `dashboard.sh` in the root folder and `config.cfg` in the `config` folder and adjust it to suit your needs. _Source code example can be found below._
- Don't forget to chmod it so that you can execute it `chmod 777 dashboard.sh`
- In the folder `layout` create a php-file for each custom wallpaper you want. These files should output your HTML. _The name of the file must be the same as your folder you created earlier. This will be the argument that you specify below in `dashboard.sh`._
- Create a cron job that runs your `dashboard.sh` every X-minutes. _You can run the script manually by runing `./dashboard.sh` in your prefered terminal._ 
- Look in your wallpaper folder. If everything works, you should have two images in each folder. Change your desktop background and don't forget to check the `Change picture:`
- Enjoy

##Directory structure

- `/config` your `config.cfg` containing phantomjs path and wallpaper path
- `/layout` contains different HTML/CSS/JS libraries and such
- `/output` contains all your different desktops as images

## dashboard.sh

    source ./config/config.cfg

    # Get date
    DATE=`date +"%Y%m%d%H%M%S"`

    # Generic function for creating bg:s
    # Accepts one argument
    function generateImage {
        
        # Create base layout based on the created PHP-file
        php ./layouts/$1.php > cache/$1.html

        # Args for create-image.js fileName width height
        # Adjust image dimensions according to your screen resolution
        $phantomjs_path --load-images=true create-image.js $1 $2 $3

        rm $wallpaper_path/$1/*
        
        cp output/$1.png $wallpaper_path/$1/$1${DATE}.png
        cp output/$1.png $wallpaper_path/$1/$1${DATE}1.png

    }

    ###################
    # Generate images #
    ###################
    generateImage example_desktop 1280 1024

##config.cfg

    phantomjs_path="/usr/local/bin/phantomjs"
    wallpaper_path="~/Documents/Wallpapers"

##Cron job on Mac

In terminal:

- `crontab -e`
- Add a new line `*/3 * * * *  cd ~/git/dashboard && ./dashboard_example.sh >/dev/null 2>&1` and update the paths to match your project


------

##Todo

- [ ] Create a more robust structure for setting up multiple desktop executions
- [ ] Adjust script to enable use of online webpages(use layout as public folder)
- [ ] Write some clever widget-mechanism with settings for how often the script is going to be executed
- [ ] Add error-message to `query_http` if no cache exists and internet connection is down
