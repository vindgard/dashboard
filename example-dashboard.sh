source ./config/config.cfg

# Get date
DATE=`date +"%Y%m%d%H%M%S"`

# Generic function for creating bg:s
# Accepts three arguments
# $1 = dashboardName
# $2 = width in pixels
# $3 = height in pixels
function generateImage {

    # Create screenshot
    /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome \
        --headless \
        --disable-gpu \
        --screenshot=output/$1.png \
        --window-size=$2,$3 \
        --crash-dumps-dir=/tmp \
        file:///$dashboard_path/cache/$1.html \
        --virtual-time-budget=2000

    # Delete old data
    rm -Rf $wallpaper_path/$1/*

    # Create two versions
    cp output/$1.png $wallpaper_path/$1/$1${DATE}.png
    cp output/$1.png $wallpaper_path/$1/$1${DATE}1.png

}

###################
# Generate images #
###################
generateImage example-desktop 800 600