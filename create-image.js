var system = require('system');
var args = system.args;

if (args.length === 1) {

	// No args
	console.log('Try to pass some arguments when invoking this script!');
	phantom.exit();

} else {
	
	// Settings
	var fileName = args[1];
	var iWidth = args[2];
	var iHeight = args[3];
	
	// Initialize
	var page = require('webpage').create();
	page.viewportSize = { width: iWidth, height: iHeight };
	page.clipRect = { top: 0, left: 0, width: iWidth, height: iHeight };

	page.open('cache/'+fileName+'.html', function(status) {

		
		if(status === "success") {
			page.render('output/'+fileName+'.png',{format: 'png'});
		}
		
		phantom.exit();
	})
}